Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  get "/meals", to: "meals#index"
  get "/meals/:id", to: "meals#show"
  post "/meals", to: "meals#create"
  put "/meals/:id", to: "meals#update"
  patch "/meals/:id", to: "meals#update"
  delete "/meals/:id", to: "meals#destroy"
  
  get "/meal_categories", to: "meal_categories#index"
  post "/meal_categories", to: "meal_categories#create"
  delete "/meal_categories/:id", to: "meal_categories#destroy"
  put "/meal_categories/:id", to: "meal_categories#update"
  patch "/meal_categories/:id", to: "meal_categories#update"
  
  get "/order/index", to: "orders#index"
  get "/order/:id", to: "orders#show"
  post "/order", to: "orders#create"
  put "/order/:id", to: "orders#update"
  patch "/order/:id", to: "orders#update"
  delete "/order/:id", to: "orders#destroy"
  get "order/:id/order", to: "orders#find"


  get "/situation/index", to: "situations#index"
  post "/situation/create", to: "situations#create"
  delete "/situation/:id", to: "situations#destroy"


  get "/users", to: "users#index"
  get "users/:id", to: "users#show"
  get "users/:id/bill", to: "users#bill"
  post "/users", to: "users#create"
  post '/auth/login', to: 'authentication#login'
  delete "/users/:id", to: "users#destroy"
  put "users/:id", to: "users#update"
  patch "users/:id", to: "users#update"

  
  get '/*', to: 'application#not_found'


  get "order-meal/index", to: "order_meals#index"
  get "order-meal/:id", to: "order_meals#show"

  post "order-meal/", to: "order_meals#create"

  put "order-meal/:id", to: "order_meals#update"
  patch "order-meal/:id", to: "order_meals#update"

  delete "order-meal/:id", to: "order_meals#destroy"


end
