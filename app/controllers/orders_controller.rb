class OrdersController < ApplicationController

    before_action :authorize_request
    before_action :set_order ,only: [:show, :update, :destroy, :find]

    #GET "/order/index"
    def index
        if @current_user.admin
            @orders = Order.all
            if @orders
                render json: @orders, status: 200
            else
                render json: @orders.errors, status: :unprocessable_entity
            end
        else
            render json: @current_user.admin, status: 401
        end
    end

    #GET "order/:id/order"
    def find
        if @order = order_total()
            render json: @order, status: 200
        else
            redner json: @order.errors, status: 200
        end
    end


    #GET "order/:id"
    def show
        if @current_user.admin
            if @orders
                render json: @orders, status: 200
            else
                render json: @orders.errors, status: :unprocessable_entity
            end
        else
            render json: @current_user.admin, status: 401
        end
    end

    #POST "/order"
    def create
        @orders = Order.new(user_id: @current_user.id)
        if @orders.save
            render json: @orders, status: 200
        else
            render json: @orders.errors, status: :unprocessable_entity
        end
    end


    #PUT/PATCH "/order/:id"
    def update
        if @current_user.admin
            # Garante q o id da situação passado está entre os existentes
            if params[:situation_id] > 3 or params[:situation_id] < 1
                render json: @orders.errors, status: 403
            else
                if @orders.update(situation_id: params[:situation_id])
                    render json: @orders, status: 200
                else
                    render json: @orders.errors, status: :unprocessable_entity
                end
            end
         else
            render json: @current_user.admin, status: 401
        end
    end

    #DELETE "order/:id"
    def destroy
        if @orders.destroy
            render json: @orders, status: 200
        else
            render json: @orders.erros, status: :unprocessable_entity
        end
    end

    private

        def set_order
            @orders = Order.find(params[:id])
        end

        def order_params
            params.require(:order).permit(:user_id)
        end

        # Função que filtra informações da order do usuario passada como parâmetro
        def order_total
            @order_array = []
            @orders.order_meal.each do | order_meal |
                @data = {"prato": order_meal.meal.name,
                     "quantidade": order_meal.quantity,
                    "status": @orders.situation.description} #TEM QUE POR SITUAÇÃO
                @order_array << @data
            end
            @order_array << {"total": @orders.price}    
        end
    end