class OrderMealsController < ApplicationController
    before_action :authorize_request
    before_action :set_order_meal, only: [:show, :destroy, :update]

    # GET "order-meal/index"
    def index
        if @current_user.admin
            if @order_meals = OrderMeal.all
                render json: @order_meals, status: 200
            else
                render json: @order_meals.errors, status: 200
            end
        else
            render json: @current_user.admin, status: 401
        end
    end

    #GET "order-meal/:id"
    def show
        if @order_meals
            render json: @order_meals, status: 200
        else
            render json: @order_meals.erros, staus: 200
        end
    end

    #POST "order-meal/"
    def create
        @order_meals = OrderMeal.new(order_meal_params)
        if @order_meals.save
            render json: @order_meals, status: 200
        else
            render json: @order_meals.errors, status: :unprocessable_entity
        end
    end

    #PUT/PATCH "order-meal/:id"
    def update
        if @order_meals.update(order_meal_params)
            render json: @order_meals, status: 200
        else
            render json: @order_meals.errors, status: :unprocessable_entity
        end
    end


    #DELETE "order-meal/:id"
    def destroy
        if @order_meals.destroy
            render json: @order_meals, status: 200
        else
            render json: @order_meals.errors, status: :unprocessable_entity
        end
    end



    private
        def set_order_meal
            @order_meals = OrderMeal.find(params[:id])
        end

        def order_meal_params
            params.require(:order_meal).permit(:quantity, :order_id, :meal_id)
        end


end
