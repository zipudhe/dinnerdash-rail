include Rails.application.routes.url_helpers

class MealsController < ApplicationController
    before_action :set_meal, only: [:show, :update, :destroy]
    before_action :authorize_request, except: [:index, :show]

    

    #GET "/meals"
    def index
        @meals = Meal.all
        render json:@meals, status: 200
    end

    #GET "/meals/:id"
    def show
        render json: {:meal=> @meal, :image => "http://localhost:3000#{@meal.picture_url}"}, status: 200
    end

    #POST "/meals"
    def create
        if @current_user.admin
            @meal = Meal.new(meal_params)
            if @meal.save
                render json:@meal, status: 200
            else
                render json:@meal.errors, status: :unprocessable_entity
            end
        else
            render json: @current_user.admin, status: 401
        end
    end

    #PUT/PATCH "/meals/:id"
    def update
        if @current_user.admin
            if @meal.update(meal_params)
                render json: @meal, status: 200
            else
                render json: @meal.errors, status: :unprocessable_entity
            end
        else
            render json: @current_user.admin, status: 401
        end
    end

    #DELETE "/meals/:id"
    def destroy
        if @current_user.admin
            if @meal.destroy
                render json: @meal, status: 200
            end
        else
            render json: @current_user.admin, status: 401
        end
    end

    private

        def set_meal
            @meal = Meal.find(params[:id])
        end

        def meal_params
            params.require(:meal).permit(:id, :name, :description, :price, :available, :meal_category_id, :image)
        end

        
end