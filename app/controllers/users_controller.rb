class UsersController < ApplicationController

    before_action :authorize_request, except: [:create]
    before_action :set_user, only: [:destroy, :update, :show, :bill]

    #GET "/users"
    def index
        @q = User.ransack(params[:q])
        if @user = @q.result(distinct: true)
            render json: @user, status: 200
        else
            render json: @user.erros, status: 200
        end
    end

    #GET "/users/:id"
    def show
        @q = User.ransack(id_eq: @users.id)
        if @user = @q.result(distinct: true)
            render json: @user, status: 200
        else
            render json: @user.erros, status: 200
        end
    end

    #GET "users/:id/bill"
    def bill
        if @bill = get_bill()
            render json: @bill, status: 200
        else
            render json: @bill.errors, status: :unprocessable_entity
        end
    end

    #POST "/users"
    def create
        @users = User.new(set_users_params)
        if @users.save
            render json: @users, status: 200
        else
            render json: @users.errors, status: 200
        end
    end

    # PUT /users/:id
    def update
        if @users = User.update(set_users_params)
            render json: @users, status: 200
        else
            render json: @users.errors, status: :unprocessable_entity
        end
    end

    #DELETE "/users/:id"
    def destroy
        if @users.destroy
            render json: @users, status: 200
        else
            render json: @users.erros, status: 200
        end
    end


    private 

        def set_user
            @users = User.find(params[:id])
        end

        def set_users_params
            params.require(:user).permit(:name, :username, :email, :password, :password_confirmation, :admin)
        end

        # Função que itera por todas as orders e OrdersMeal do usuario
        # e pega informações e soma os preços de cada um
        def get_bill
            @meal_array = []
            @bill = []
            @total = 0 
            @users.orders.each do | order |
                order.order_meal.each do | order_meal |
                    @data = {"nome": order_meal.meal.name,
                             "descricao": order_meal.meal.description,
                            "preco": order_meal.meal.price,
                            "quantidade": order_meal.quantity}
                    @meal_array << @data
                end
                @total += order.price
            end
            @gorjeta = @total * 0.1
            @meal_array << {"total": @total, "gorjeta": @gorjeta.round(2)}
    
        end

end
