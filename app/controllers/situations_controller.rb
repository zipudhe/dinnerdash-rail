class SituationsController < ApplicationController
    before_action :authorize_request
    before_action :set_situations, only: [:destroy]
    
    #GET "/situation/index"

    def index 
        if @current_user.admin 
            @situations = Situation.all
            if @situations
                render json: @situations, status: 200
            else
                render json: @situations.errors, status: :unprocessable_entity
            end
        else
            render json: @current_user.admin, status: 401
        end

    end

    #POST "/situation/create"
    def create
        if @current_user.admin
            @situations = Situation.new(situation_params)
            if @situations.save
                render json: @situations, status: 200
            else
                render json: @situations.errors, stauts: :unprocessable_entity
            end
        else
            render json: @current_user.admin, status: 401
        end
    end

    #DELETE "/situation/:id"
    def destroy
        if @current_user.admin
            if @situations.destroy
                    render json: @situations, status: 200
                else
                    render json: @situations.errors, status: :unprocessable_entity
                end
        else
            render json: @current_user.admin, status: 401
        end
    end

    private
        def set_situations
            @situations = Situation.find(params[:id])
        end    
    
        def situation_params
            params.require(:situation).permit(:description)
        end      

    end

