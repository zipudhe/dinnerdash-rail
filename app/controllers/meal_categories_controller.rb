class MealCategoriesController < ApplicationController
    before_action :set_meal_categories, only: [:destroy, :update]
    before_action :authorize_request, except: [:index]

    #GET "/meal_categories"
    def index
        @meal_categories = MealCategory.all
        render json:@meal_categories, status: 200
    end

    #POST "/meal_categories"
    def create
        if @current_user.admin
            @meal_categories = MealCategory.new(id: params[:id], name: params[:name])
            if @meal_categories.save
                render json:@meal_categories, status: 200
            else
                render json:@meal_categories.errors, status: :unprocessable_entity
            end
        else
            render json: @current_user.admin, status: 401
        end
    end

    #DELETE "/meal_categories/:id"
    def destroy
        if @current_user.admin
            if @meal_categories.destroy
                render json: @meal_categories, status: 200
            end
        else
            render json: @current_user.admin, status: 401
        end
    end

    #PUT/PATCH "/meal_categories/:id"
    def update
        if @current_user.admin
            if @meal_categories.update(name: params[:name])
                render json: @meal_categories, status: 200
            else
                render json: @meal_categories.errors, status: :unprocessable_entity
            end
        else
            render json: @current_user.admin, status: 401
        end
    end

    private

        def set_meal_categories
            @meal_categories = MealCategory.find(params[:id])
        end
end
