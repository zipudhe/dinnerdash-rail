class MealCategory < ApplicationRecord
    validates :name, presence: true
    validates :name, length:{ maximum: 45}

    has_many :meals
end
