class User < ApplicationRecord
    validates :name, presence: true
    has_secure_password
    validates :email, presence: true, uniqueness: true
    validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
    validates :username, presence: true, uniqueness: true
    validates :password,
              length: { minimum: 6 },
              if: -> { new_record? || !password.nil? }

    has_many :orders, dependent: :destroy

    after_create :set_order

    def set_order
        Order.create(user_id: self.id)
    end

end