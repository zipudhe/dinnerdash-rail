class Meal < ApplicationRecord
    validates :name, presence: true
    validates :name, length:{ maximum: 45}
    validates :description, presence: true
    validates :description, length:{ maximum: 250}

    belongs_to :meal_category
    has_one_attached :image

    def picture_url
        rails_blob_url(self.image, only_path: true) if self.image.attached?
    end

    # after_create :add_price, :add_situation

    # def add_price
    #     meal = Meal.find(self.id)
    #     meal.price = "R$0,00"
    #     meal.save
    # end

    # def add_situation
    #     meal = Meal.find(self.id)
    #     meal.situation_id = 1
    #     meal.save
    # end
end
