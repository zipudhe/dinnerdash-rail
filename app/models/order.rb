class Order < ApplicationRecord
    after_initialize :set_default
    before_update :set_price

    validates :situation_id, presence: false
    has_many :order_meal, dependent: :destroy
    belongs_to :situation, optional: true
    belongs_to :user

    #funcao para definir preco padrão na inicialização
    def set_default
        if self.price == nil
            self.price = 0.0
            self.situation_id = 1
        end
    end

    def set_price
        @price = 0
        self.order_meal.each do | order_meal |
            @price = ( order_meal.quantity * order_meal.meal.price ) + @price
        end
        self.price = @price
    end


end
