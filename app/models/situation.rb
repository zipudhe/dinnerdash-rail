class Situation < ApplicationRecord
    validates :description, presence: true, length: { maximum: 55 }
    has_many :orders
end
