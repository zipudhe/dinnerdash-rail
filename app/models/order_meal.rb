class OrderMeal < ApplicationRecord
  validates :quantity, presence: true
  validates :order_id, presence: true
  validates :meal_id, presence: true
  belongs_to :order
  belongs_to :meal

  after_update :update_order
  after_destroy :update_order
  after_create :update_order


  def update_order
    self.order.update(situation_id: self.order.situation_id)
  end

end
