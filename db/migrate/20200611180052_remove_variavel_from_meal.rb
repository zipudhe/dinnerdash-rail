class RemoveVariavelFromMeal < ActiveRecord::Migration[6.0]
  def change
    remove_column :meals, :available, :string
  end
end
