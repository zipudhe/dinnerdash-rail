class RemoveOrderFromSituation < ActiveRecord::Migration[6.0]
  def change
    remove_reference :situations, :order, null: false, foreign_key: true
  end
end
