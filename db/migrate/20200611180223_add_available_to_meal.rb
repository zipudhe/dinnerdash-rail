class AddAvailableToMeal < ActiveRecord::Migration[6.0]
  def change
    add_column :meals, :available, :boolean
  end
end
