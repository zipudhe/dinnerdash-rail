class AddOrderToSituation < ActiveRecord::Migration[6.0]
  def change
    add_reference :situations, :order, null: false, foreign_key: true
  end
end
