class AddOrderMealsToMeal < ActiveRecord::Migration[6.0]
  def change
    add_reference :meals, :order_meals, null: false, foreign_key: true
  end
end
