class RemovePriceFromMeal < ActiveRecord::Migration[6.0]
  def change
    remove_column :meals, :price, :string
  end
end
