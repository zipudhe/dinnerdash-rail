class RemoveOrderMealsFromMeal < ActiveRecord::Migration[6.0]
  def change
    remove_reference :meals, :order_meals, null: false, foreign_key: true
  end
end
